import { Component,ChangeDetectorRef, OnInit   } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { ViewChild } from '@angular/core';
import { Porteur } from './porteur';
import { DemandeDSDS } from './demande-dsds';
//import { jsPDF } from "jspdf";
//import html2canvas from 'html2canvas';  
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;


export interface CodeEtab{
  code:string;
}

export interface RefrencielGroup{
  libRef:string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{

  @ViewChild('stepper') private stepper: MatStepper;
  title = 'dsdsApp';
  etranger:boolean = false;
  dsdsSent:boolean = false;
  isPorteurSearch:boolean = true;
  isLinear = false;
  dsdsForm:FormGroup;
  firstFormGroup: FormGroup;
  creaForm:FormGroup;
  secondFormGroup: FormGroup;
  currPorteur:Porteur = new Porteur('','','','','','','','');
  currDSDS:DemandeDSDS = new DemandeDSDS('','',[],[],'','','','');
  condition:boolean = false;
  message:string='';
  messNotExist = "Cette personne n'existe pas dans le système - Veuillez créer un nouveau porteur";
  messNotMatch = "Si le porteur ne figure pas dans cette liste - Veuillez créer un nouveau porteur";
  porteursFound:Porteur[] = Array();
  //communesFound:Commune[] = Array();
  code: string;
  cat:string;
  portDE:string;
  portEXT:string;
  portBAG:string;
  portCRI:string;
  expoY:string;
  expoX:string;
  expoB:string;
  expoN:string;
  donneesEtab:string;
  surveillance:string;
  debSuivi:string;
  finSuivi:string;

  //////////////// Validation porteur

  submitted = false;

  typValid:boolean = true;
  nomValid:boolean = true;
  usNomValid:boolean = true;
  prenomValid:boolean = true;
  natValid:boolean = true;
  inseeValid:boolean = true;
  naissValid:boolean = true;
  comValid:boolean = true;

  //////////////// Validation demande dsds

  codeValid:boolean = true;
  catValid:boolean = true;
  portValid:boolean = true;
  donneeValid:boolean = true;
  expoValid:boolean = true;
  surValid:boolean = true;
  datValid:boolean = true;

  ////////////////////////////////// FAKE DATA

  public groupes:RefrencielGroup[]=[
    {libRef:'REF A1'},
    {libRef:'REF A2'},
    {libRef:'REF A3'},
    {libRef:'REF B1'},
    {libRef:'REF B2'},
    {libRef:'REF B3'},
    {libRef:'REF C1'},
    {libRef:'REF C2'},
    {libRef:'REF C3'},
    {libRef:'REF D1'},
    {libRef:'REF E'},
    {libRef:'REFERENCE F'},
    {libRef:'REFERENCE G'},
    {libRef:'REFERENCE H'},
    {libRef:'REFERENCE I'}
  ]

  public codes:CodeEtab[]=[
    {code:'1234AB'},
    {code:'5627ZS'},
    {code:'1234DL'},
    {code:'1256ML'},
    {code:'7415CV'},
    {code:'7946WE'},
    {code:'4598JI'},
    {code:'1235HJ'},
    {code:'4596BN'},
    {code:'7748YU'},
    {code:'7596FT'},
    {code:'1278QZ'}
  ]

  public porteurs:Porteur[] = [
    {nom: "Cornillac", prenom: 'Clovis', insee: '170081535962',etab:'56956652',debSuivi:'',finSuivi:'',datNaiss:'12/07/1967',villNaiss:'Quimper'},
    {nom: "Renard", prenom: 'Colette', insee: '220056335962',etab:'124566',debSuivi:'14/09/1920',finSuivi:'14/09/1921',datNaiss:'15/11/1920',villNaiss:'Paris'},
    {nom: "Gainsbourg", prenom: 'Charlotte', insee: '278081535962',etab:'3622952',debSuivi:'08/04/2016',finSuivi:'',datNaiss:'22/10/1979',villNaiss:'Paris'},
    {nom: "Dujardin", prenom: 'Jean', insee: '169081535962',etab:'',debSuivi:'16/08/2007',finSuivi:'',datNaiss:'16/03/1971',villNaiss:'Saint-Etienne'},
    {nom: "Tautou", prenom: 'Audray', insee: '278081535962',etab:'4383838',debSuivi:'02/10/2019',finSuivi:'',datNaiss:'14/06/82',villNaiss:'Paris'},
    {nom: "Piaf", prenom: 'Édith', insee: '210081535962',etab:'95956',debSuivi:'01/05/1940',finSuivi:'',datNaiss:'17/12/1905',villNaiss:'Paris'},
    {nom: "Adjani", prenom: 'Isabelle', insee: '267081535962',etab:'42852',debSuivi:'20/10/2018',finSuivi:'02/10/2022',datNaiss:'14/07/1970',villNaiss:'Paris'},
    {nom: "Aznavour", prenom: 'Clovis', insee: '170081535962',etab:'4172275',debSuivi:'',finSuivi:'',datNaiss:'18/11/1816',villNaiss:'Allauch'},
    {nom: "Brubeck", prenom: 'Dave', insee: '170081535962',etab:'58369',debSuivi:'',finSuivi:'',datNaiss:'09/06/1911',villNaiss:''},
    {nom: "Baker", prenom: 'Chet', insee: '170081535962',etab:'18256',debSuivi:'',finSuivi:'',datNaiss:'12/08/1652',villNaiss:'Martigues'},
    {nom: "Cocker", prenom: 'Joe', insee: '170081535962',etab:'74359',debSuivi:'',finSuivi:'',datNaiss:'14/07/1986',villNaiss:'Cuers'},
    {nom: "Hendrix", prenom: 'Jimmy', insee: '170081535962',etab:'97643',debSuivi:'',finSuivi:'',datNaiss:'17/08/1667',villNaiss:'Tarbes'},
    {nom: "Joplin", prenom: 'Janis', insee: '170081535962',etab:'14259',debSuivi:'',finSuivi:'',datNaiss:'15/07/1924',villNaiss:'Rougiers'},
    {nom: "Brel", prenom: 'Jacques', insee: '156081589661',etab:'4172275',debSuivi:'',finSuivi:'',datNaiss:'13/09/1958',villNaiss:'Vauvenargues'},
    {nom: "Lopez", prenom: 'Jennyfer', insee: '268081535954',etab:'58369',debSuivi:'',finSuivi:'',datNaiss:'',villNaiss:''},
    {nom: "Auteuil", prenom: 'Daniel', insee: '163081535962',etab:'18256',debSuivi:'20/05/2018',finSuivi:'20/10/2024',datNaiss:'',villNaiss:''},
    {nom: "Stone", prenom: 'Sharon', insee: '262081535962',etab:'74359',debSuivi:'17/03/2016',finSuivi:'',datNaiss:'',villNaiss:''},
    {nom: "Theron", prenom: 'Charlize', insee: '261081535962',etab:'97643',debSuivi:'24/04/2017',finSuivi:'',datNaiss:'',villNaiss:''},
    {nom: "Johansson", prenom: 'Scarlett', insee: '275081535962',etab:'14259',debSuivi:'10/05/2013',finSuivi:'',datNaiss:'',villNaiss:''},
    {nom: "Cordula", prenom: 'Christina', insee: '268081535954',etab:'58369',debSuivi:'12/10/2001',finSuivi:'',datNaiss:'12/08/1967',villNaiss:'Mazaugues'},
    {nom: "Lamy", prenom: 'Alexandra', insee: '270130826652274',etab:'18256',debSuivi:'20/05/2018',finSuivi:'20/10/2024',datNaiss:'17/01/1988',villNaiss:'Martigues'},
    {nom: "Brassens", prenom: 'Georges', insee: '138051652856884',etab:'74359',debSuivi:'17/03/2016',finSuivi:'',datNaiss:'',villNaiss:''},
    {nom: "Manaudou", prenom: 'Laure', insee: '261081535962',etab:'97643',debSuivi:'24/04/2017',finSuivi:'',datNaiss:'01/10/1985',villNaiss:'Perpignan'},
    {nom: "Cordy", prenom: 'Annie', insee: '242536912556887',etab:'26538',debSuivi:'24/04/2017',finSuivi:'',datNaiss:'01/07/1942',villNaiss:'Boulogne'},
    {nom: "Moustaki", prenom: 'Georges', insee: '152243625686524',etab:'14259',debSuivi:'10/05/2013',finSuivi:'',datNaiss:'',villNaiss:''}
];

////////////////////


  constructor(private _formBuilder: FormBuilder) { 

  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  findPorteurs(searchValue: string): void { 
    this.porteursFound.splice(0,this.porteursFound.length);
    if(searchValue == '')
    {
      this.condition = false;
      return;
    }
      for(var i = 0; i < this.porteurs.length; i++) {
        if (this.porteurs[i].nom.toUpperCase().startsWith(searchValue.toUpperCase())) {
            this.porteursFound.push(this.porteurs[i]);  
        }
      }
    if(this.porteursFound.length == 0){
      this.condition = true;
      this. message = this.messNotExist;
    }
    else{
      this.condition = true;
      this. message = this.messNotMatch;
    }
  }

  handleChange(nom:string,prenom:string,insee:string,etab:string,debS:string,finS:string,dat:string,vill:string){
    //mettre a jour le message
    this. message = '';
    //modifier le porteur courant
    this.currPorteur.nom = nom;
    this.currPorteur.prenom = prenom;
    this.currPorteur.insee = insee;
    this.currPorteur.etab = etab;
    this.currPorteur.debSuivi = debS;
    this.currPorteur.finSuivi = finS;
    this.currPorteur.datNaiss = dat;
    this.currPorteur.villNaiss = vill;
  }

  handleNationalite(data:string){
    switch(data){
      case 'france':
        this.etranger = false;
        break;
      case 'etranger':
        this.etranger = true;
        break;
    }
  }

  creaPorteurView(){
    this.isPorteurSearch = false;
  }

  backToSearch(){
    this.isPorteurSearch = true;
  }

  backToSearchView(){
    this.stepper.previous();
  }

  onSubmit(data:any){
    this.submitted = true;
     //validation 
     if(data.ckporteurType == null)
     {
       this.typValid = false;
       return;
     }
     if(data.nom == null)
     {
       this.nomValid = false;
       return;
     }
     if(data.usNomPorteur == null)
     {
       this.usNomValid = false;
       return;
     }
     if(data.prenom == null)
     {
       this.prenomValid = false;
       return;
     }
     if(data.ckporteurNat == null)
     {
      this.natValid = false;
      return;
     }
     if(!this.etranger && (data.ssNum == null || data.ssCle == null))
     {
      this.inseeValid = false;
      return;
     }
     if(data.ssNum != null && data.ssCle != null){
      var reg ='^([1-37-8])([0-9]{2})(0[0-9]|[2-35-9][0-9]|[14][0-2])((0[1-9]|[1-8][0-9]|9[0-69]|2[abAB])(00[1-9]|0[1-9][0-9]|[1-8][0-9]{2}|9[0-8][0-9]|990)|(9[78][0-9])(0[1-9]|[1-8][0-9]|90))([0-9]{3})([0-8][0-9]|9[0-7])';
      if(!(data.ssNum + data.ssCle).match(reg)){
        this.inseeValid = false;
        return;
      }
     }
     if(data.datNaissPorteur == null)
     {
      this.naissValid = false;
      return;
     }
     if(data.datNaissPorteur != null)
     {
       // regular expression to match required date format
       var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
       if (!data.datNaissPorteur.match(re))
       {
          this.naissValid = false;
          return;
       }
     }
     if(!this.etranger && data.comNaissPorteur == null)
     {
      this.comValid = false;
      return;
     }
     

    //mettre a jour le porteur courant
    this.currPorteur.nom = data.nom;
    this.currPorteur.prenom = data.prenom;
    this.currPorteur.insee = data.ssNum+data.ssCle;
    this.currPorteur.datNaiss = data.datNaissPorteur;
    this.currPorteur.villNaiss = data.comNaissPorteur;
     //enregistrer provisoirement le porteur
    var pushed = {'nom':data.nom, 'prenom':data.prenom, 'insee':data.ssNum+data.ssCle,'etab':'','debSuivi':'','finSuivi':'','datNaiss':data.datNaissPorteur,'villNaiss': data.comNaissPorteur};
    this.porteurs.push(pushed);
    //rediriger vers la demande DSDS
    this.stepper.next();
  }

  creaDemandeDSDSView(){
    //rediriger vers le formulaire de creation DSDS
    this.stepper.next();
  }

  saveDemandeDSDS(data:any){
     //validation TODO 
    if(data.code == null || data.code ==''){
      this.codeValid = false;
      return;
    }
    if(data.cat == null){
      this.catValid = false;
      return;
    }
    if(data.portDE == null && data.portEXT == null && data.portCRI == null && data.portBAG == null
      ||(!data.portDE && !data.portEXT && !data.portBAG && !data.portCRI)){
      this.portValid = false;
      return;
    }
    if(data.expoY == null && data.expoX == null && data.expoB == null && data.expoN == null 
      ||(!data.expoY && !data.expoX && !data.expoB && !data.expoN)){
      this.expoValid = false;
      return;
    }
    if(data.donneesEtab == null){
      this.donneeValid = false;
      return;
    }
    if(data.surveillance == null){
      this.surValid = false;
      return;
    }
    if(data.debSuivi == null){
      this.datValid = false;
      return;
    }
    if(data.debSuivi != null)
    {
      // regular expression to match required date format
      var re = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
      if (!data.debSuivi.match(re))
      {
         this.datValid = false;
         return;
      }
    }
    //enregistrer provisoirement la demande DSDS
    this.currDSDS.code = data.code;
    this.currDSDS.categorie = data.cat;
    if(data.portDE){
    this.currDSDS.port.push({type:"Poitrine(DE)"});
    }
    if(data.portEXT){
    this.currDSDS.port.push({type:"Poignet(EXT)"});
    }
    if(data.portBAG){
    this.currDSDS.port.push({type:"Bague(BAG)"});
    }
    if(data.portCRI){
    this.currDSDS.port.push({type:"Cristallin(CRI)"});
    }
    if(data.expoY){
    this.currDSDS.expo.push({ray:"Y"});
    }
    if(data.expoX){
    this.currDSDS.expo.push({ray:"X"});
    }
    if(data.expoB){
    this.currDSDS.expo.push({ray:"B"});
    }
    if(data.expoN){
    this.currDSDS.expo.push({ray:"Neutrons"});
    }
    this.currDSDS.donneesEtab = data.donneesEtab;
    this.currDSDS.surveillance = data.surveillance;
    this.currDSDS.debSuivi = data.debSuivi;
    this.currDSDS.finSuivi = data.finSuivi;
    //rediriger vers la confirmation de la creation de la demande DSDS
    this.dsdsSent = true;
  }

  initErrorCtrl(){
    this.typValid = true;
    this.nomValid = true;
    this.usNomValid = true;
    this.prenomValid = true;
    this.natValid = true;
    this.inseeValid = true;
    this.naissValid = true;
    this.comValid = true;
    this.codeValid = true;
    this.catValid = true;
    this.portValid = true;
    this.expoValid = true;
    this.donneeValid = true;
    this.surValid = true;
    this.datValid = true;
  }

  async downloadPdf(){
  var dd = {
    content: [
      {
        text: 'sira',
        style: 'header',
        alignment: 'center'
      },
      {
        image: await this.getBase64ImageFromURL(
          "../assets/images/image.png"
      ),
        width: 60,
        style:'logo'
      },
      {
        text: '\nDSDS: Nouvelle demande',
        style: 'header',
        alignment: 'left',
        margin: [0, 120, 0, 80]
      },
      {
        text: [
          '\nÉtablissement: HIA Percy (Radiologie)[6101]\n',
          'PCR: PM Jean Dujardin'
          ],
        style: 'header',
        bold: false
      }
      ,
      {
        text: [
          '\n' + this.currPorteur.nom + ' ' + this.currPorteur.prenom + ' né(e) le ' 
          + this.currPorteur.datNaiss +  ' à ' + this.currPorteur.villNaiss + '\n',
          '\n'
          ],
        bold: false
      },
      {
        text: [
          '\nCode emploi:' + this.code + '\n',
          ],
        bold: false
      },
      '\nCatégorie',
      {
        table: {
          body: [
            ['A', 'B', 'Non catégorisé'],
            (this.cat == 'A')?['X', '', '']:(this.cat == 'B')?['', 'X', '']:['', '', 'X']
          ]
        }
      },
      ,
      '\nPort',
      {
        table: {
          body: [
            ['Poitrine(DE)', 'Poignet(EXT)', 'Bague(BAG)','Christallin(CRI)'],
            [(this.portDE != null)?'X':'',(this.portEXT != null)?'X':'',
            (this.portBAG != null)?'X':'',(this.portCRI != null)?'X':'']
          ]
        }
      },
      ,
      '\nExposition',
      {
        table: {
          body: [
            ['Y', 'X', 'B','Neutrons'],
            [(this.expoY != null)?'X':'',(this.expoX != null)?'X':'',
            (this.expoB != null)?'X':'',(this.expoN != null)?'X':'']
          ]
        }
      },
      {
        text: [
          '\nDonnées établissement:' + this.donneesEtab + '\n',
          ],
        bold: false
      },
      {
        text: [
          '\nSurveillance souhaitée:' + this.surveillance + '\n',
          ],
        bold: false
      },
      ,
      {
        text: [
          '\nDate de début: ' + this.debSuivi + '\n',
          ],
        bold: false
      },
      ,
      {
        text: [
          (this.finSuivi != null)? '\nDate de fin: ' + this.finSuivi + '\n':'',
          ],
        bold: false
      },
    ],
    styles: {
      header: {
        fontSize: 14,
        bold: true,
        alignment: 'justify'
      },
      logo: {
        position: 'absolute',
        alignment: 'center'
      }
    }
    
  }
  pdfMake.createPdf(dd).open();
}

  getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
      var img = new Image();
      img.setAttribute("crossOrigin", "anonymous");
      img.onload = () => {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        resolve(dataURL);
      };
      img.onerror = error => {
        reject(error);
      };
      img.src = url;
    });
  }

}
