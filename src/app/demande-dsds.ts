export class DemandeDSDS{
    code:string;
    categorie:string;
    port:any[];
    expo:any[];
    donneesEtab:string;
    surveillance:string;
    debSuivi:string;
    finSuivi:string;
    constructor(code:string,cat:string,port:[],expo:[],
      donneesEtab:string,surveillance:string,debSuivi:string,finSuivi:string) {
      this.code = code;
      this.categorie = cat;
      this.port = port;
      this.expo = expo;
      this.donneesEtab = donneesEtab;
      this.surveillance = surveillance;
      this.debSuivi = debSuivi;
      this.finSuivi = finSuivi;
    }
  }